﻿using Discount.Grpc.Protos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Discount.Grpc.Protos.DiscountProtoService;

namespace Basket.Api.GrpcServices
{
    public class DiscountGrpcService
    {
        private readonly DiscountProtoServiceClient _serviceClient;

        public DiscountGrpcService(DiscountProtoServiceClient serviceClient)
        {
            _serviceClient = serviceClient ?? throw new ArgumentNullException(nameof(serviceClient));
        }

        public async Task<CouponModel> GetDiscount(string productName)
        {
            var coupon = await _serviceClient.GetDiscountAsync(new GetDiscountRequest
            {
                ProductName = productName
            });

            return coupon;
        }
    }
}
